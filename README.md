# Xeros13
Repositorio Oficial de Fire Media
<br>
<br>

<img src="https://gitlab.com/Xeros13/fire-media/raw/master/icon.png">





# **Images:**
![]()
![]()
![]()
![]()

Pagina Oficial: http://www.xeros13.tk/ <br>
Twitter: https://twitter.com/Xer0s13 <br>
Correo: <br>
Repositorio Oficial: https://github.com/Xeros13/Fire-Media

<h1> Colaborar </h1>
<h3>Si deseas colaborar con el proyecto eres bienvenido a hacerlo, pero ten en cuenta por favor estas pautas

*Para colaborar con sus Pull Request se pide seguir unas reglas de estilo basadas en las de python PEP8. Esto se puede hacer desde diferentes IDEs que lo traen integrado como pydev, pycharm o ninjaIDE, o extensiones que se pueden agregar a por ejemplo sublime, atom.

*Utilizar nombre de clases, métodos y variables en inglés para favorecer la comprensión de personas no hispano parlantes.</h3>